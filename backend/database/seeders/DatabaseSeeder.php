<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

use App\Models\Hotel;
use App\Models\Ticket;
use App\Models\Coupon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call([
            AdminTableSeeder::class,
        ]);

        Hotel::truncate();
        Ticket::truncate();
        Coupon::truncate();

        DB::table('hotels')->insert([
            'name' => 'Hotel Marina Port',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Et nemo obcaecati doloremque, vero natus impedit, fuga sapiente fugiat non suscipit pariatur distinctio? Quisquam veritatis ratione blanditiis quidem! Nostrum provident ducimus ipsum nemo consequatur asperiores iusto atque culpa excepturi vitae aut ut aliquam ratione rem voluptatibus quos, illo vel! Ut, aliquid!',
            'city' => 'Balatonkenese',
            'stars' => 4,
            'image' => 'marina-port.jpg',
            'rooms' => 50
        ]);

        DB::table('hotels')->insert([
            'name' => 'Világos Hotel Balatonvilágos',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Et nemo obcaecati doloremque, vero natus impedit, fuga sapiente fugiat non suscipit pariatur distinctio? Quisquam veritatis ratione blanditiis quidem! Nostrum provident ducimus ipsum nemo consequatur asperiores iusto atque culpa excepturi.',
            'city' => 'Balatonvilágos',
            'stars' => 3,
            'image' => 'vilagos-hotel.jpg',
            'rooms' => 30
        ]);

        DB::table('hotels')->insert([
            'name' => 'Kenese-Völgye Villahotel',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Et nemo obcaecati doloremque, vero natus impedit, fuga sapiente fugiat non suscipit pariatur?',
            'city' => 'Balatonkenese',
            'stars' => 3,
            'image' => 'kenese-volgye.jpg',
            'rooms' => 5
        ]);

        DB::table('tickets')->insert([
            'code' => '001',
            'name' => 'Kombinált jegy',
            'price' => 135000,
            'price_discount' => 108000,
            'amount' => 100
        ]);

        DB::table('tickets')->insert([
            'code' => '002',
            'name' => 'Regatta nézői jegy',
            'price' => 95000,
            'price_discount' => 76000,
            'amount' => 100
        ]);

        DB::table('tickets')->insert([
            'code' => '003',
            'name' => 'Konferencia nézői jegy',
            'price' => 55000,
            'price_discount' => 44000,
            'amount' => 100
        ]);


        $coupons = [
            2840,
            2839,
            2838,
            2837,
            2836,
            2835,
            2834,
            2833,
            2832,
            2831,
            2830,
            2847,
            2846,
            2845,
            2844,
            2843,
            2848,
            2855,
            2849,
            2857,
            2858,
            2862,
            2860,
            2863,
            2864,
            2865,
            2866,
            2868,
            2869,
            2897,
            2904,
            2907,
            2913,
            2912,
        ];

        foreach ($coupons as $coupon) {
            DB::table('coupons')->insert([
                'code' => $coupon,
            ]);
        }
    }
}
