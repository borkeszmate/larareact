<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class AdminTableSeeder extends DatabaseSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Máté Borkesz',
            'email' => 'borkeszmate@gmail.com',
            'password' => Hash::make('test'),
			'sort_order' => '1'
        ]);
    }
}
