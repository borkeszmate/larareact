<?php
use Illuminate\Foundation\Application;
use Illuminate\Support\Env;
use Dotenv\Dotenv;

function overloadDotenv(Application $app): void
{
    $app->useEnvironmentPath(dirname(base_path()));

    $app->afterLoadingEnvironment(function ($app) {
        $environment = getenv('APP_ENV');


        if (in_array($environment, ['master', 'staging', 'testing'], true)) {
            $dotenv = Dotenv::create(
                Env::getRepository(),
                $app->environmentPath(),
                '.env.' . $environment
            );
            $dotenv->load();
        }
    });
}
