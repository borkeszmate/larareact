<?php

namespace App\Http\Requests;

use App\Requests\FormRequests;

use App\Models\Production;

class ProductionDeleteRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => 'required|numeric',
        ];
    }
}
