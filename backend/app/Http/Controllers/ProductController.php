<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Product;

class ProductController extends BaseController
{
    public $productions;

    public function getProducts():JsonResponse
    {
        $productions = Product::all();
        return response()->json($productions);
    }
}
