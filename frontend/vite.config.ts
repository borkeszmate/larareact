import * as path from 'path';
import strip from '@rollup/plugin-strip';
import { UserConfig } from 'vite';
import vitePlugins from './vite.plugins';

const isMaster = process.env.NODE_ENV === 'production';
// const proxyUrl = 'http://' + path.basename(path.dirname(__dirname)) + '.test';

// console.log('proxy: ' + proxyUrl);

const stripConsoleLogs = (
	isMaster ?
		strip({
			include: '**/*.{js,ts}',
		}) :
		undefined
);

const config: UserConfig = {
	server: {
		port: 4200,

		// proxy: {
		// 	'/api': {
		// 		ws: false,
		// 		target: proxyUrl,
		// 		changeOrigin: true,
		// 	},
		// },
	},

	plugins: vitePlugins,

	resolve: {
		alias: {
			'/@': path.join(__dirname, 'src'),
		},
	},

	build: {
		outDir: '../public',
		assetsDir: 'assets',

		target: 'es2015',
		cssCodeSplit: true,
		minify: true,
		sourcemap: false,
		manifest: false,

		rollupOptions: {
			plugins: [
				stripConsoleLogs,
			],
		},
	},

	css: {
		preprocessorOptions: {
			scss: {
				includePaths: [
					path.join(__dirname, 'src/styles'),
					path.join(__dirname, 'node_modules'),
				],
			},
		},
	},

	optimizeDeps: {
		exclude: [
			'x----x----x',
		],
	},
};

export default config;
