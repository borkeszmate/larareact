import react from '@vitejs/plugin-react'
import { minify } from 'html-minifier';
import { Plugin } from 'vite';
import viteImagemin from 'vite-plugin-imagemin';
import { ViteWebfontDownload } from 'vite-plugin-webfont-dl';

const isMaster = process.env.NODE_ENV === 'production';
const reactPlugin = react();

const imageMinPlugin: Plugin = (
	isMaster ?
		viteImagemin({
			filter: /\.svg$/i,
			gifsicle: false,
			webp: false,
			mozjpeg: false,
			pngquant: false,
			optipng: false,
			svgo: {
				plugins: [
					{
						removeViewBox: false,
					},
					{
						cleanupIDs: false,
					},
				],
			},
		}) :
		undefined
);


const minifyHtmlPlugin: Plugin = {
	name: 'minifyHtml',
	apply: 'build',
	enforce: 'post',
	transformIndexHtml: (html) => {
		if (!isMaster) {
			return html;
		}

		return minify(html, {
			collapseWhitespace: true,
			minifyCSS: true,
			minifyJS: true,
			removeAttributeQuotes: true,
			removeComments: true,
		});
	},
};

const viteWebfontDownload = ViteWebfontDownload([
	'https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600&display=swap',
	'https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;900&display=swap'
]);


export default [
	reactPlugin,
	minifyHtmlPlugin,
	imageMinPlugin,
	viteWebfontDownload
];
