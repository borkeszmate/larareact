import axios from 'axios';
import ApiRequestError from './interceptors/ApiRequestError';
import ApiRequestInterceptor from './interceptors/ApiRequestInterceptor';
import ApiResponseError from './interceptors/ApiResponseError';
import ApiResponseInterceptor from './interceptors/ApiResponseInterceptor';
import LanguageRequestInterceptor from './interceptors/LanguageRequestInterceptor';
import StagingRequestInterceptor from './interceptors/StagingRequestInterceptor';

const Api = axios.create({
	// baseURL: 'https://example.hu/api',
	baseURL: '/api',
	// baseURL: import.meta.env.VITE_ENV === 'master' ? '/api' : 'https://example.staging.borkeszweb.hu/api',
});

Api.interceptors.request.use(ApiRequestInterceptor, ApiRequestError);
Api.interceptors.request.use(LanguageRequestInterceptor, ApiRequestError);
Api.interceptors.request.use(StagingRequestInterceptor, ApiRequestError);

Api.interceptors.response.use(ApiResponseInterceptor, ApiResponseError);

export default Api;
