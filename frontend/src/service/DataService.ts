import ProductsRequests from './requests/ProductsRequests';

export default {
	products: ProductsRequests,
};
