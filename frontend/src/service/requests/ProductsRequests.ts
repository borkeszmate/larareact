import Api from '../Api';


const ProductsRequests = {
	getProductions: () => {
		return Api.get('/products');
	},
};

export default ProductsRequests;
