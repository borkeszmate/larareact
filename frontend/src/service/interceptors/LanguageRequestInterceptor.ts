const LanguageRequestInterceptor = (config: any) => {
	const language = localStorage.getItem('language');

	if (language !== null && language !== '') {
		config.headers['Content-Language'] = language.toLowerCase();
	}

	return config;
};

export default LanguageRequestInterceptor;
