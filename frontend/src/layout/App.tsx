import { Route, Routes } from 'react-router-dom';
import Header from '/@/components/Header/Header';
import Home from '/@/pages/Home/Home';
import Dummy from '/@/pages/Dummy/Dummy';
import './App.scss';

const App = () => {
  return (
	<main>
		<Header></Header>
		<Routes>
			<Route path="/" element={<Home />}></Route>
			<Route path="/dummy" element={<Dummy />}></Route>
		</Routes>
	</main>
  )
}

export default App
