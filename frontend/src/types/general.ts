export interface Img {
	'1x': string;
	'2x': string;
	'1x_webp': string;
	'2x_webp': string;
	lqip?: string;
	mobile?: string;
	mobile_webp?: string;
	thumb?: string;
	thumb_webp?: string;
	aspect_ratio?: string;
}
