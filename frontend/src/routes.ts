import Home from '/@/pages/Home'

export const routes = [
	{
	  component: Home,
	  routes: [
		{
		  path: "/",
		  exact: true,
		  component: Home
		},
	  ]
	}
];
