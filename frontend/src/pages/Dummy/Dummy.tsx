import { useReducer } from "react";

const Dummy = () => {

	const initialState = { count: 0 };

	const reducer = (state: any, action: any) => {
		switch (action.type) {
			case 'increment':
				return { count: state.count + 1 };
			case 'decrement':
				return { count: state.count - 1 };
			default:
				throw new Error();
		}
	}

	const [state, dispatch] = useReducer(reducer, initialState);

	return (
		<>
			<div className="page">
				<div className="container">
					<h1>Dummy page</h1>
					Count: {state.count}
					<button onClick={() => dispatch({ type: 'decrement' })}>-</button>
					<button onClick={() => dispatch({ type: 'increment' })}>+</button>
				</div>
			</div>
		</>
	)
}

export default Dummy;

