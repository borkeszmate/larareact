import { useEffect, useRef, useContext, useState } from 'react'
import Dataservice from '/@/service/DataService';
import './Home.scss'
import img from '/@/assets/img/dummy/dummy.jpg'

type Props = {}

const Home = ({ }: Props) => {
	const [products, setProducts] = useState<any[]>();


	useEffect(() => {
		console.log('useeffect');
		Dataservice.products.getProductions()
			.then(res => {
				setProducts(res?.data);
			})
			.catch(err => {
				console.log(err);
			})
	}, []);

	const onChildEmit = (arg: any) => {
		console.log(arg);
	};

	return (
		<div className="page home">
			<div className="container">
			<img src={img} alt="" />
				{products?.map((item: any) =>
					<div key={item.id}>{item.name}</div>
				)}
			</div>
		</div>
	)
}

export default Home
