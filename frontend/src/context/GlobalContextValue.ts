import ResponsiveImg from '/@/components/ResponsiveImg/ResponsiveImg';

const GlobalContextValue = {
	ResponsiveImg,
};

export default GlobalContextValue;
