import 'lazysizes';
import { Img } from '/@/types/general';
import { useState, useRef, useEffect } from 'react';
import './ResponsiveImg.scss'


type Props = {
	img: Img,
};

const ResponsiveImg = (props: Props) => {
	const imgRef = useRef<any>();

	// const emit = defineEmits(['pictureLoaded']);


	const [failed, setFailed] = useState<boolean>(false);
	const [alt, setAlt] = useState<string>('');
	const [thumbnail, setThumbnail] = useState<boolean>(false);
	const [fallback, setFallback] = useState<boolean>(false);

	const dpr = window.devicePixelRatio;
	const emptyGif = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';

	const [src, setSrc] = useState<string>('');
	const [dataSrc, setDataSrc] = useState<string>('');
	const [dataSrcsetWebp, setDataSrcsetWebp] = useState<string>('');

	let calculateTimeout: any;
	let windowInnerWidth = window.innerWidth;
	const isPrerender: boolean = (window.navigator.userAgent.indexOf('HeadlessChrome') !== -1);

	setSrc(emptyGif);

	const onPictureError = () => {
		if (src && fallback && !isPrerender) {
			setFailed(true);
			setSrc(emptyGif);
			setDataSrc('');
			setDataSrcsetWebp('');
		}
	};

	useEffect(() => {
		console.log(imgRef.current);
		calculateSrc();
	},[imgRef]);


	const calculateSrc = (): void => {
		setFailed(false);

		removeClasses();

		const isDesktop = window.innerWidth >= 768;

		if (isPrerender) {
			 setSrc(isDesktop ? getDesktopSrc() as string : getMobileSrc() as string);
		} else {
			if (props.img.lqip) {
				setSrc(props.img.lqip);
			}

			setDataSrc(isDesktop ? getDesktopSrc() as string : getMobileSrc() as string);
		}

		setDataSrcsetWebp(isDesktop ? getDesktopWebpSrcset() as string : getMobileWebpSrcset() as string);

		if (!isPrerender) {
			setTimeout(() => addClasses());
		}
	};


const getDesktopSrc = (): string | undefined => {
	if (thumbnail && props.img.thumb) {
		return props.img.thumb;
	}

	if (dpr > 1 && props.img['2x']) {
		return props.img['2x'];
	}

	return props.img['1x'];
};

const getMobileSrc = (): string | undefined => {
	if (thumbnail && props.img.thumb) {
		return props.img.thumb;
	}

	return props.img.mobile || getDesktopSrc();
};

const getDesktopWebpSrcset = (): string => {
	const srcset = [];

	if (thumbnail && props.img.thumb_webp) {
		srcset.push(props.img.thumb_webp);
	} else {
		if (dpr > 1 && props.img['2x_webp']) {
			srcset.push(props.img['2x_webp'] + ' 2x');
		}

		if (props.img['1x_webp']) {
			srcset.push(props.img['1x_webp'] + ' 1x');
		}
	}

	return srcset.join(', ');
};

const getMobileWebpSrcset = (): string | undefined => {
	if (thumbnail && props.img.thumb_webp) {
		return props.img.thumb_webp;
	}

	if (props.img.mobile_webp) {
		return props.img.mobile_webp;
	}

	if (!props.img.mobile) {
		return getDesktopWebpSrcset();
	}
};

const addClasses = (): void => {
	const imgElement: HTMLElement = imgRef.current as HTMLElement;

	if (imgElement) {
		if (props.img.lqip) {
			imgElement.classList.add('blur');
		}

		imgElement.classList.add('lazyload');
		imgElement.classList.add('lazyloading');
	}
};

	const removeClasses = (): void => {
		const imgElement: HTMLElement = imgRef.current as HTMLElement;

		if (imgElement) {
			imgElement.classList.remove('blur');
			imgElement.classList.remove('lazyload');
			imgElement.classList.remove('lazyloading');
			imgElement.classList.remove('lazyloaded');
		}
	};

	const style = (): { [key: string]: string } => {
		const style: { [key: string]: string } = {};

		if (props.img.aspect_ratio) {
			style.aspectRatio = props.img.aspect_ratio;
		}

		return style;
	};

	return (

			<picture style={style()} className={failed ? 'fuseState' : ''}>

				{dataSrcsetWebp && <source data-srcset="dataSrcsetWebp" type="image/webp"/>}

				<img
					ref={imgRef}
					src={src}
					data-src={dataSrc}
					alt={alt}
					style={style()}
					draggable="false"
					// @load="onPictureLoaded($event)"
					// @error="onPictureError()"
					/>

				{failed &&
					<div className="fallback">
						🚫
						{alt && <div>{ alt }</div>}
					</div>
				}

			</picture>
	);
}

export default ResponsiveImg;

