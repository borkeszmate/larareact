type Props = {
	name: string,
	obj: any,
	emit: any,
}

const DummyComponent = (props: Props) => {
	const onClick = () => {
		props.emit(Math.random() * 10);
	}

	return (
		<>
			<h1>I am dummy component</h1>
			<h2>Prop: {props.name}</h2>
			<h3>Prop: {props.obj.valami}</h3>

			<button onClick={onClick}>Emit from child</button>
		</>
	)
}

export default DummyComponent;
